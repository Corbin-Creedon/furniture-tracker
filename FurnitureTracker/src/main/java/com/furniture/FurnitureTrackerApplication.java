package com.furniture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FurnitureTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FurnitureTrackerApplication.class, args);
	}

}
