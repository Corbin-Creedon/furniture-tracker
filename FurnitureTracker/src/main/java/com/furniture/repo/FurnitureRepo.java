package com.furniture.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.furniture.model.Furniture;


public interface FurnitureRepo extends JpaRepository<Furniture, Integer>{
	
	public List<Furniture> findAll();
	public Furniture findByName(String name);
	public Furniture findByFurnitureId(int id);

}
