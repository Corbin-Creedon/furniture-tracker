package com.furniture.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.furniture.model.*;


public interface HomeRepo extends JpaRepository<Home, Integer>{

	public List<Home> findAll();
	public Home findByName(String name);
	public Home findByHomeId(int id);
}
