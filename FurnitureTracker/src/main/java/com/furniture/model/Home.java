package com.furniture.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="home_records")
public class Home {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "home_id")
	private int homeId;
	
	@Column( nullable = false)
	private int area;
	
	@Column(name = "available_area")
	private int availableArea;
	
	@Column(nullable = false, unique = true)
	private String name;
	
	@OneToMany(mappedBy = "home",cascade=CascadeType.ALL)
	private List<Furniture> ownedFurniture;

	public Home(int homeId, int area, int availableArea, String name, List<Furniture> ownedFurniture) {
		super();
		this.homeId = homeId;
		this.area = area;
		this.availableArea = availableArea;
		this.name = name;
		this.ownedFurniture = ownedFurniture;
	}

	public Home(int area, int availableArea, String name, List<Furniture> ownedFurniture) {
		super();
		this.area = area;
		this.availableArea = availableArea;
		this.name = name;
		this.ownedFurniture = ownedFurniture;
	}

	public Home(int area, String name) {
		super();
		this.area = area;
		this.name = name;
	}

	public Home() {
		super();
	}

	public int getHomeId() {
		return homeId;
	}

	public void setHomeId(int homeId) {
		this.homeId = homeId;
	}

	public int getArea() {
		return area;
	}

	public void setArea(int area) {
		this.area = area;
	}

	public int getAvailableArea() {
		return availableArea;
	}

	public void setAvailableArea(int availableArea) {
		this.availableArea = availableArea;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Furniture> getOwnedFurniture() {
		return ownedFurniture;
	}

	public void setOwnedFurniture(List<Furniture> ownedFurniture) {
		this.ownedFurniture = ownedFurniture;
	}

	@Override
	public String toString() {
		return "Home [homeId=" + homeId + ", area=" + area + ", availableArea=" + availableArea + ", name=" + name
				+ ", ownedFurniture=" + ownedFurniture + "]";
	}

	
	
	

}
