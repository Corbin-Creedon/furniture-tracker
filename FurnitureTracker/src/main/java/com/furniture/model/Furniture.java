package com.furniture.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "furniture_records")
public class Furniture {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "furniture_id")
	private int furnitureId;
	
	@Column( nullable = false)
	private int size;
	
	@Column(nullable = false, unique = true)
	private String name;
	
	@ManyToOne
    @JoinColumn(name= "in_home_id")
	@JsonIgnore
	private Home home;

	public Furniture(int furnitureId, int size, String name, Home home) {
		super();
		this.furnitureId = furnitureId;
		this.size = size;
		this.name = name;
		this.home = home;
	}

	public Furniture() {
		super();
	}

	public Furniture(int size, String name, Home home) {
		super();
		this.size = size;
		this.name = name;
		this.home = home;
	}

	public Furniture(int furnitureId, int size, String name) {
		super();
		this.furnitureId = furnitureId;
		this.size = size;
		this.name = name;
	}
	
	

	public Furniture(int size, String name) {
		super();
		this.size = size;
		this.name = name;
	}

	public int getFurnitureId() {
		return furnitureId;
	}

	public void setFurnitureId(int furnitureId) {
		this.furnitureId = furnitureId;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Home getHome() {
		return home;
	}

	public void setHome(Home home) {
		this.home = home;
	}

	@Override
	public String toString() {
		return "Furniture [furnitureId=" + furnitureId + ", size=" + size + ", name=" + name + ", home=" + home.getName() + "]";
	}
	
	
	
	
}
