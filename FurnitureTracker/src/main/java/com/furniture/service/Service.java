package com.furniture.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.furniture.model.Furniture;
import com.furniture.model.Home;
import com.furniture.repo.FurnitureRepo;
import com.furniture.repo.HomeRepo;

public class Service {
	
	private HomeRepo homeRepo;
	private FurnitureRepo furnRepo;
	
	public boolean evaluateSize(Home home, Furniture furniture) {
		int availRoom = home.getAvailableArea();
		int size = furniture.getSize();
		if (availRoom<size) {
			return false;
		}
		else {
			return true;
		}
	}
	

	
	public HomeRepo getHomeRepo() {
		return homeRepo;
	}

	public void setHomeRepo(HomeRepo homeRepo) {
		this.homeRepo = homeRepo;
	}

	public FurnitureRepo getFurnRepo() {
		return furnRepo;
	}

	public void setFurnRepo(FurnitureRepo furnRepo) {
		this.furnRepo = furnRepo;
	}

	@Autowired
	public Service(HomeRepo homeRepo, FurnitureRepo furnRepo) {
		super();
		this.homeRepo = homeRepo;
		this.furnRepo = furnRepo;
	}

	public Service() {
		super();
	}



	
}
