package com.furniture.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.furniture.model.Furniture;
import com.furniture.model.Home;
import com.furniture.repo.FurnitureRepo;
import com.furniture.repo.HomeRepo;
import com.furniture.service.Service;

@RestController
public class Controller {
	
	private HomeRepo homeRepo;
	private FurnitureRepo furnRepo;
	
	private Service service = new Service();
	
	
	@GetMapping(path = "/allHomes",produces = "application/json")
	public List<Home> findAllHomes(){
		return homeRepo.findAll();
	}
	
	@GetMapping(path = "/home/{homeId}",produces = "application/json")
	public Home findHome(@PathVariable int homeId){
		return homeRepo.findByHomeId(homeId);
	}
	
	@PostMapping(path = "/home")
	public void addHome(@RequestBody Home home) {
		home.setAvailableArea(home.getArea()/2);
		homeRepo.save(home);
	}
	
	//BL
	@PostMapping(path = "/home/{homeId}")
	public String addFurniture(@PathVariable int homeId , @RequestBody Furniture furniture) {
		Home home = homeRepo.findByHomeId(homeId);
		if (service.evaluateSize(home,furniture)) {
		furniture.setHome(home);
		furnRepo.save(furniture);
		home.setAvailableArea(home.getAvailableArea()-furniture.getSize());
		homeRepo.save(home);
		return "Good!";
		}
		else {
			return "The furniture is too dang big!";
		}
	}
	
	//BL
	@PutMapping(path = "/home/{homeId}")
	public void updateHome(@PathVariable int homeId , @RequestBody Home home) {
		home.setHomeId(homeId);
		homeRepo.save(home);
	}
	
	//BL
	@PutMapping(path = "/home/{homeId}/furniture/{furnitureId}")
	public void updateFurniture(@PathVariable int homeId,@PathVariable int furnitureId , @RequestBody Furniture furniture) {
		furniture.setFurnitureId(furnitureId);
		Home home = homeRepo.findByHomeId(homeId);
		furniture.setHome(home);
		furnRepo.save(furniture);
	}
	
	@DeleteMapping(path = "/home/{homeId}")
	public void deleteHome(@PathVariable int homeId) {
		Home home = homeRepo.findByHomeId(homeId);
		homeRepo.delete(home);
	}
	
	//BL
	@DeleteMapping(path = "/home/{homeId}/furniture/{furnitureId}")
	public void deleteFurniture(@PathVariable int homeId,@PathVariable int furnitureId ) {
		Furniture furniture = furnRepo.findByFurnitureId(furnitureId);
		furnRepo.delete(furniture);
	}

	public HomeRepo getHomeRepo() {
		return homeRepo;
	}

	public void setHomeRepo(HomeRepo homeRepo) {
		this.homeRepo = homeRepo;
	}

	public FurnitureRepo getFurnRepo() {
		return furnRepo;
	}

	public void setFurnRepo(FurnitureRepo furnRepo) {
		this.furnRepo = furnRepo;
	}

	@Autowired
	public Controller(HomeRepo homeRepo, FurnitureRepo furnRepo) {
		super();
		this.homeRepo = homeRepo;
		this.furnRepo = furnRepo;
	}

	public Controller() {
		super();
	}

	
	
	
	
	

}
